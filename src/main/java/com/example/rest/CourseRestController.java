package com.example.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import com.example.service.CourseService;
import com.example.model.CourseModel;

import java.util.List;

@RestController
@RequestMapping("/rest")
public class CourseRestController {
	@Autowired
	CourseService courseService;

	@RequestMapping("/course/view/{id}")
	public CourseModel view(@PathVariable(value = "id") String id) {
		CourseModel course = courseService.selectCourseStudent(id);
		return course;
	}
	
	@RequestMapping("/course/viewall")
    public List<CourseModel> viewall() {
        List<CourseModel> courses = courseService.selectAllCourses();
        return courses;
    }

}